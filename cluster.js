
'use strict';

const koa = require('koa');
const router = require('@koa/router')();
const cluster = require('cluster'); // built in
const os = require('os'); // built in

const numCPUs = os.cpus().length;
const port = 3000;

// cluster is thread
// isMaster deprecated
if ( cluster.isPrimary ) {
    console.log(`This machine has ${numCPUs} CPUs`); 
    console.log(`Master node ${process.pid} is running`); 

    for (let i = 0; i < numCPUs; i++) {
        cluster.fork(); // create a separate thread
    }

    cluster.on('exit', (worker, code, signal) => {
        console.log(`Worker ${worker.process.pid} died`);
        console.log("Let's fork another worker!");
        cluster.fork();
    });
} else {

    // do this for all clusters 
    // reference: https://blog.appsignal.com/2021/02/03/improving-node-application-performance-with-clustering.html
    const app = new koa();

    router.get('/', (ctx) => {
        ctx.body = {
            message: 'Access / get route'
        };
    });

    router.get('/worker/:id', (ctx) => {

        let id = parseInt(ctx.params.id);
        let count = 0;

        // limit is 5 billion
        if (id > 5000000000) id = 5000000000;
        
        // simulating long running operation
        // add sum from 0 to id
        
        for (let i = 0; i <= id; i++) {
            count += i;
        }
    
        ctx.body = { 
            message: `Final count is ${count}`
        };
    });
    
    app.use(router.routes());

    // all workers share the same port
    app.listen(port, () =>  console.log(`Worker node ${process.pid} => http://localhost:${port}/`));

}
  

